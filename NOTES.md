**Correct SFS responses when (an) error(s) occur(s)**

**SFS returns 500 error when dataflows have not yet been indexed**

**Don't index dataflows (or their characteristics) that do not have the localized name**

**Limitations for the indexing of dimensions according to NOT_INDEXED annotation**

**Correct SFS responses when (an) error(s) occur(s) for admin/dataflow(s)**

* remove lock when tenant has no datasource and enhance logs
* add error message when datasource cannot be found
* refactor all debug messages
* refactor http response status code depending on sfs indicators


**Improve value of 'Last updated' field**

* add lastUpdated
* replace all usages of indexationDate by lastUpdated
* keep indexationDate has it to get real indexation date!


**Harmonise organisationId/tenant and dataspaceID/spaceId**

* replace 'spaceId' by 'dataSpaceId' in logs admin api
* replace 'spaceId' by 'dataSpaceId' in dataflow admin api
* for both keep compatibility with spaceId usage
* replace organisationId by tenant in logs


