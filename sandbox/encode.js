const { times, propEq, toString, compose, trim } = require('ramda');
const baseX = require('base-x');

const BASE62 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const bs62 = baseX(BASE62);
const encode = compose(x => bs62.encode(Buffer.from(x)), trim);
const decode = compose(toString, x => bs62.decode(x));

times(() => encode('JKHJKHJHJHJKHJHJHJ'), 10000);
