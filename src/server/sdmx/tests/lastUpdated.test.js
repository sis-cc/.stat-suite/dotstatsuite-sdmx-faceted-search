import Parser, { getLastUpdated } from '../dataflow'
import DF from '../../tests/mocks/sdmx/dataflow_sample'

const defaultParserOptions = { datasourceId: 'TEST', dimensionValuesLimit: 1000 }

describe('SDMX | dataflow', () => {
  const ref = { agencyID: 'A', id: 'DF', version: '1.0' }
  it('should make dataflow', () => {
    const structure = DF(ref)
    structure.data.contentConstraints[0].validTo = '2015-02-11T11:30:30'
    const parser = Parser(structure, defaultParserOptions)
    const dataflow = parser.transpile(ref)
    expect(dataflow.hasNoContraints).toBeTruthy()
    expect(dataflow.fields.lastUpdated.getFullYear()).toEqual(2017)
  })

  it('should get lastUpdated from contentConstraint', () => {
    const cc = [
      {
        validFrom: '2017-02-11T10:30:30.000Z',
      },
    ]
    const lastUpdated = getLastUpdated(cc)
    expect(lastUpdated.getFullYear()).toEqual(2017)
  })

  it('should get lastUpdated from dataflow', () => {
    const cc = [{}]
    const dataflow = {
      annotations: [
        {
          type: 'LAST_UPDATE',
          text: '2018-02-11T10:30:30.000Z',
        },
      ],
    }
    const lastUpdated = getLastUpdated(cc, dataflow)
    expect(lastUpdated.getFullYear()).toEqual(2018)
  })

  it('should get lastUpdated from date.now()', () => {
    const cc = [{}]
    const dataflow = {}
    const lastUpdated = getLastUpdated(cc, dataflow)
    expect(lastUpdated.getFullYear()).toEqual(new Date().getFullYear())
  })

  it('should get lastUpdated from now', () => {
    const cc = [
      {
        validFrom: '2010-02-11T10:30:30.000Z',
        validTo: '2011-02-11T10:30:30.000Z',
      },
    ]
    const lastUpdated = getLastUpdated(cc)
    expect(lastUpdated > new Date(2018, 1, 1)).toBeTruthy()
  })
})
