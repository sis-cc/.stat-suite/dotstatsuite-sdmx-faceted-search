import { greaterThan, getParentId } from '../utils';

describe('SDMX | utils', () => {
  it('should compare version', () => {
    expect(greaterThan('1.1.1', '1.1.0')).toBeTruthy();
    expect(greaterThan('1.2.1', '1.1.0')).toBeTruthy();
    expect(greaterThan('2.2.1', '1.3.0')).toBeTruthy();
    expect(greaterThan('2.', '1.3.0')).toBeTruthy();
    expect(greaterThan('1.1.1', '1.2.0')).toBeFalsy();
  });

  it('should return parentId', () => {
    expect(getParentId({ 11: 1 }, 11)).toEqual(1);
    expect(getParentId({ 111: 11, 11: 1 }, 111)).toEqual(1);
    expect(getParentId({ 1: undefined }, 1)).toEqual(undefined);
  });
});
