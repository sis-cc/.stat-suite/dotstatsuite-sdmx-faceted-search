import debug from '../../debug'
import { reduce } from 'ramda'
import evtX from 'evtx'
import initHealthcheck from './healthcheck'
import initHealthcheckMongo from './healthcheckMongo'
import initHealthcheckSOLR from './healthcheckSOLR'
import initHealthcheckSearch from './healthcheckSearch'

const allServices = [initHealthcheck, initHealthcheckMongo, initHealthcheckSOLR, initHealthcheckSearch]

const initServices = evtx => reduce((acc, service) => acc.configure(service), evtx, allServices)

export default async ctx => {
  const healthcheck = evtX(ctx).configure(initServices)
  debug.info('Healthcheck up.')
  return ctx({ services: { ...ctx().services, healthcheck } })
}
