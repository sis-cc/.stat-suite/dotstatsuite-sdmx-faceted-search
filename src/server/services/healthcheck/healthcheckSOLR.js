const NAME = 'healthcheckSOLR'

async function doHealthcheck() {
  const { solr } = this.globals()

  await solr.status()
  return { status: 'OK' }
}

const healthcheck = {
  get: doHealthcheck,
}

export default evtx => evtx.use(NAME, healthcheck)
