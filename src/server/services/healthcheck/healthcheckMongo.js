const NAME = 'healthcheckMongo'

async function doHealthcheck() {
  const { mongo } = this.globals()

  await mongo.ping()
  return { status: 'OK' }
}

const healthcheck = {
  get: doHealthcheck,
}

export default evtx => evtx.use(NAME, healthcheck)
