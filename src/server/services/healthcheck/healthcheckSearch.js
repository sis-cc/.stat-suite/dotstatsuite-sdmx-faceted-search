import { HEALTHCHECK_COLLECTION } from '../../solr'

const NAME = 'healthcheckSearch'

async function doHealthcheck() {
  const { solr } = this.globals()

  const solrClient = solr.getClient({ id: HEALTHCHECK_COLLECTION })
  await solrClient.select({ query: '*:*' })
  return { status: 'OK' }
}

const healthcheck = {
  get: doHealthcheck,
}

export default evtx => evtx.use(NAME, healthcheck)
