import { HTTPError } from '../utils/errors'
import { LANGS } from '../sdmx/utils'
import { includes, split, defaultTo } from 'ramda'

export const checkTenant = async ctx => {
  const { tenant } = ctx.locals
  if (!tenant) throw new HTTPError(400, 'Tenant is required!')
  return ctx
}

export const getSolrConfig = async ctx => {
  const { configManager } = ctx.globals()
  const { tenant } = ctx.locals
  const solrConfig = await configManager.getUpdatedConfig(tenant)
  return { ...ctx, locals: { ...ctx.locals, solrConfig } }
}

export const getSolrClient = async ctx => {
  const { solr } = ctx.globals()
  const { tenant } = ctx.locals
  return { ...ctx, locals: { ...ctx.locals, solrClient: solr.getClient(tenant) } }
}

export const getFacetOrders = async ctx => {
  const { configManager } = ctx.globals()
  const { tenant } = ctx.locals
  const facetOrders = configManager.getOrders(tenant)
  return { ...ctx, locals: { ...ctx.locals, facetOrders } }
}

export const checkLang = async ctx => {
  const langs = split(',', LANGS)
  const lang = defaultTo(ctx.locals?.solrConfig?.defaultLocale, ctx.input?.lang)
  if (!includes(lang, langs)) {
    throw new HTTPError(400, `Unsupported lang: ${lang} not found in ${LANGS}`)
  }
  return ctx
}