import HTTPStatus from 'http-status';
import { HTTPError } from '../../utils/errors';

export const checkApiKey = ctx => {
  const {
    req: {
      headers: { 'x-api-key': xApiKey },
      query: { 'api-key': qApiKey },
    },
  } = ctx.locals;
  const {
    config: { apiKey },
  } = ctx.globals();
  if (apiKey !== (xApiKey || qApiKey)) return Promise.reject(new HTTPError(HTTPStatus.UNAUTHORIZED));
  return Promise.resolve(ctx);
};

export const getStatusCode = data => {
  let errors = 0
  let indexed = 0
  for(const ds of Object.values(data)){
    errors += ds.dataflows?.errors ?? 0
    indexed += ds.dataflows?.indexed ?? 0
  }
  if (indexed) return errors ?  207 : 200
  return 409 
}
