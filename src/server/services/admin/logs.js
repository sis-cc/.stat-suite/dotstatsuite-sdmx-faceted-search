import { checkTenant } from '../utils'

const NAME = 'logs'

export const service = {
  async post(params) {
    const { models } = this.globals()
    const { res, tenant } = this.locals
    const result = await models.jobs.loadAll(tenant, params)
    if (params.id) res.json(result[0])
    return result
  },
}

service.get = service.post

const init = evtx => {
  evtx.use(NAME, service)
  evtx.service(NAME).before({
    post: [checkTenant],
  })
}

export default init
