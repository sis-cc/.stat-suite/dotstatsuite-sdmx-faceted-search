import evtX from 'evtx'
import { tearContext, initConfig, manageSchema, SYNONYMS, STOPWORDS } from './utils'
import initSolr from '../solr'
import initMongo from '../init/mongo'
import initSearchService from '../services/api/search'
import initConfigService from '../services/api/config'
import { checkTenant, getSolrClient, getSolrConfig } from '../services/utils'
import { omit, pluck } from 'ramda'

const data = [
  {
    id: 'IRS',
    type_s: 'dataflow',
    datasourceId_ss: ['SOURCE1'],
    agency_s: 'ECB',
    version_s: '1.0',
    name_sfs_text_en: 'fly tulip',
    sname_en_s: 'A',
    name_sfs_text_it: 'B',
  },
  {
    id: 'NSA',
    type_s: 'dataflow',
    datasourceId_ss: ['SOURCE1'],
    agency_s: 'FBI',
    version_s: '1.0',
    name_sfs_text_en: 'ground petunia',
    sname_en_s: 'A',
    name_sfs_text_it: 'B',
  },
]

const configFactory = lang => ({
  dataflows: {
    outFields: ['id', 'agency', 'version', 'name'],
    searchFields: ['id', 'agency', 'name'],
    schema: [
      {
        name: 'sname',
        field: lang => `sname_${lang}_s`,
      },
      {
        name: 'name',
        field: lang => `name_sfs_text_${lang}`,
      },
      {
        name: 'sortName',
        field: lang => `sortName_sfs_text_sort_${lang}`,
      },
      {
        name: 'lorder',
        field: lang => `lorder_${lang}_i`,
      },
      {
        name: 'boost',
        field: `boost_t`,
        weight: 2,
      },
      {
        name: 'toto',
        field: lang => `toto_sfs_text_${lang}`,
      },
      {
        name: 'agency',
        field: 'agency_s',
      },
      {
        name: 'version',
        field: 'version_s',
      },
      {
        name: 'reference_area',
        field: lang => `reference_area_${lang}_ss`,
        ID: 'REFERENCE_AREA',
      },
      {
        name: 'interest_rate_type',
        field: lang => `interest_rate_type_sfs_text_list_${lang}`,
      },
      {
        name: 'frequency',
        field: 'frequency_ss',
        ID: 'FREQUENCY',
      },
      {
        name: 'cat',
        field: 'cat_ss',
      },
      {
        name: 'datasourceId',
        field: 'datasourceId_ss',
        ID: 'DATASOURCEID',
      },
    ],
    facets: [
      {
        name: 'datasourceId',
        type: 'list',
        op: 'OR',
      },

      {
        name: 'frequency',
        type: 'list',
        op: 'OR',
      },
      {
        name: 'reference_area',
        type: 'list',
        op: 'OR',
      },
    ],
  },
})

const lang = 'en'
const config = configFactory({ lang })
let CTX
const tc = () => CTX
let TENANT

const localConfig = jest.fn()
const initConfigManager = ctx => {
  const configManager = { getUpdatedConfig: async () => localConfig() }
  return ctx({ configManager })
}

afterAll(tearContext(tc))

const runService = config => {
  localConfig.mockReturnValue(config)
  const api = evtX(CTX({ config }))
    .configure(initSearchService)
    .configure(initConfigService)
    .before(checkTenant, getSolrConfig, getSolrClient)
  return CTX({ api })
}

const setupContext = async catalogs => {
  CTX = await initConfig(undefined, undefined, undefined, catalogs)
  TENANT = CTX().TENANT
  await initMongo(CTX)
  await initConfigManager(CTX)
  await initSolr(CTX)
  await manageSchema(TENANT, CTX().solr)
  const solrClient = CTX().solr.getClient(TENANT)
  await solrClient.deleteAll()
  await solrClient.add(data)
}

beforeAll(async () => {
  await setupContext()
})

describe('Solr catalog management', () => {
  it('should find at a dataflow directly (witness test)', async () => {
    const input = { lang, search: '' }
    const ctx = runService(config)
    const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    expect(dataflows.length).toBe(data.length)
  })

  it('should find a dataflow directly (fly)', async () => {
    const input = { lang, search: 'fly' }
    const ctx = runService(config)
    const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    expect(pluck('id', dataflows)).toEqual(['IRS'])
  })

  it('should find a dataflow through a synonym (wave -> fly)', async () => {
    const input = { lang, search: 'wave' }
    const ctx = runService(config)
    const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    expect(pluck('id', dataflows)).toEqual(['IRS'])
  })

  it('should find a dataflow through a synonym (blow -> fly)', async () => {
    const input = { lang, search: 'blow' }
    const ctx = runService(config)
    const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    expect(pluck('id', dataflows)).toEqual(['IRS'])
  })

  it('should not find a dataflow through a synonym (wave)', async () => {
    const catalogs = { SYNONYMS: omit(['wave', 'blow'], SYNONYMS), STOPWORDS }
    await setupContext(catalogs)

    const input = { lang, search: 'wave' }
    const ctx = runService(config)
    const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    expect(dataflows.length).toBe(0)
  })

  it('should find dataflows through a synonym (flowers)', async () => {
    const input = { lang, search: 'flower' }
    const ctx = runService(config)
    const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    expect(dataflows.length).toBe(data.length)
  })
})
