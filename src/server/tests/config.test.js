import { prop } from 'ramda'
import axios from 'axios'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initCachedfacets from '../init/cachedFacets'
import initSolr from '../solr'
import initParams from '../init/loadParams'
import initConfigManager from '../init/configManager'
import initMongo from '../init/mongo'
import { manageSchema, tearContext, initConfig } from './utils'

let TENANT
let CTX
const tc = () => CTX

const initContext = async () => {
  try {
    CTX = await initConfig(createContext)
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initMongo)
      .then(initSolr)
      .then(async ctx => {
        await manageSchema(TENANT, ctx().solr)
        return ctx
      })
      .then(initConfigManager)
      .then(initParams)
      .then(initCachedfacets)
      .then(initServices)
      .then(initHttp)
    await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
  } catch (err) {
    console.error(err) // eslint-disable-line
  }
}

describe('API status service', () => {
  beforeAll(initContext)
  afterAll(tearContext(tc))

  it('should get right config', async () => {
    const { http } = CTX()
    await axios({ method: 'get', url: `${http.url}/api/config`, headers: { 'x-tenant': TENANT.id } }).then(prop('data'))
  })
})
