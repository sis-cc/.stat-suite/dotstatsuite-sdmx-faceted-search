import { createContext } from 'jeto'
import ConfigProvider from '../configProvider'
import getTenant from '../init/http/middlewares/tenant'
import nock from 'nock'

const CONFIG_URL = 'http://FAKE_URI'
const MEMBER = {
  id: 'test',
  spaces: {
    space1: {
      url: 'url1',
      headers: {
        header1: 'header1',
      },
    },
  },
}
const TENANTS = {
  [MEMBER.id]: MEMBER,
}

const SETTINGS = {
  spaces: {
    space1: {
      url: 'url2',
      headers: {
        header1: 'header11',
        header2: 'header22',
      },
    },
  },
}

const config = { configUrl: CONFIG_URL }
const configProvider = ConfigProvider(config)
const initConfig = async () => createContext({ config, configProvider })

describe('settings', () => {
  it('should not overwrite space', async () => {
    nock(CONFIG_URL)
      .get(`/configs/tenants.json`)
      .reply(200, TENANTS)
    nock(CONFIG_URL)
      .get(`/configs/test/sfs/settings.json`)
      .reply(404)
    const ctx = await initConfig()
    const req = { query: { tenant: 'test' } }
    const next = err => {
      if (err) throw err
    }
    await getTenant(ctx)(req, null, next)
    expect(req.tenant.spaces.space1.url).toEqual(MEMBER.spaces.space1.url)
    expect(req.tenant.spaces.space1.headers).toEqual(MEMBER.spaces.space1.headers)
  })

  it('should overwrite space', async () => {
    nock(CONFIG_URL)
      .get(`/configs/tenants.json`)
      .reply(200, TENANTS)
    nock(CONFIG_URL)
      .get(`/configs/test/sfs/settings.json`)
      .reply(200, SETTINGS)
    const ctx = await initConfig()
    const req = { query: { tenant: 'test' } }
    const next = err => {
      if (err) throw err
    }
    await getTenant(ctx)(req, null, next)
    expect(req.tenant.spaces.space1.url).toEqual(SETTINGS.spaces.space1.url)
    expect(req.tenant.spaces.space1.headers).toEqual(SETTINGS.spaces.space1.headers)
  })
})
