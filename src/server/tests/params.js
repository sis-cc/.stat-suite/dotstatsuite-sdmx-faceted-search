import { INT_EXT, STRING_EXT, TEXTFIELD_EXT, TEXT_EXT } from '../schema/ext.js'
import { PROP_TYPE, ID_TYPE, ATTR_TYPE } from '../schema/types.js'
import { SFS_TEXT_SORT_EXT } from '../sdmx/utils.js'

export default {
  defaultLocale: 'en',
  excludedCategorySchemeFacets: process.env.EXCLUDED_CATEGORYSCHEME_FACETS || [],
  // allow rejection of too big codelists before indexing
  dimensionValuesLimit: +process.env.DIMENSION_VALUES_LIMIT || 1000,
  fields: {
    id: {
      type: ID_TYPE,
      out: true,
      search: false,
      name: 'id',
    },
    version: {
      type: PROP_TYPE,
      ext: STRING_EXT,
      out: true,
      search: true,
      name: 'version',
    },
    externalUrl: {
      type: PROP_TYPE,
      ext: STRING_EXT,
      out: true,
      name: 'externalUrl',
    },
    agencyId: {
      type: PROP_TYPE,
      ext: STRING_EXT,
      out: true,
      highlight: true,
      search: true,
      name: 'agencyId',
    },
    dataflowId: {
      type: PROP_TYPE,
      ext: TEXTFIELD_EXT,
      out: true,
      highlight: true,
      search: true,
      localized: false,
      name: 'dataflowId',
    },
    indexationDate: {
      type: PROP_TYPE,
      ext: STRING_EXT,
      out: true,
      name: 'indexationDate',
    },
    lastUpdated: {
      type: PROP_TYPE,
      ext: STRING_EXT,
      out: true,
      name: 'lastUpdated',
    },
    name: {
      name: 'name',
      type: ATTR_TYPE,
      ext: TEXT_EXT,
      search: true,
      out: true,
      highlight: true,
      localized: true,
      weight: 2,
    },
    sname: {
      type: ATTR_TYPE,
      ext: STRING_EXT,
      name: 'sname',
      localized: true,
    },
    sortName: {
      type: ATTR_TYPE,
      ext: SFS_TEXT_SORT_EXT,
      name: 'sortName',
      localized: true,
      out: false,
      search: false,
      highlight: false,
    },
    description: {
      type: ATTR_TYPE,
      ext: TEXT_EXT,
      search: true,
      out: true,
      highlight: true,
      localized: true,
      name: 'description',
    },
    gorder: {
      // unlocalized dataflow order
      type: PROP_TYPE,
      ext: INT_EXT,
      out: false,
      name: 'gorder',
    },
    lorder: {
      // localized dataflow order
      type: ATTR_TYPE,
      ext: INT_EXT,
      out: false,
      name: 'lorder',
      localized: true,
    },
  },
}
