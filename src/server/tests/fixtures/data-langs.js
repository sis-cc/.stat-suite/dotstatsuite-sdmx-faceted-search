import R from 'ramda'

const fctName = R.compose(R.pluck('name'), R.prop('dataflows'))
const dataFTS = [
  { lang: 'en', search: 'quick', data: ['The quick brown'] },
  { lang: 'fr', search: 'ambiguë', data: ['Voix ambiguë'] },
  { lang: 'ru', search: 'Производственные', data: ['Производственные индексы'] },
]
const fixture = {
  name: 'localized free text search for exotic langs',
  data: [
    {
      id: 'DS:LFTS',
      dataflowId_s: 'LFTS',
      datasourceId_s: 'DS',
      type_s: 'dataflow',
      agencyId_s: 'SFS',
      version_s: '1.0',
      name_sfs_text_en: 'The quick brown',
      sname_en_s: 'The quick brown',
      description_sfs_text_en: 'The quick brown fox jumps over the lazy dog.',
      name_sfs_text_fr: 'Voix ambiguë',
      sname_fr_s: 'Voix ambiguë',
      description_sfs_text_fr: "Voix ambiguë d'un cœur qui, au zéphyr, préfère les jattes de kiwis",
      name_sfs_text_ru: 'Производственные индексы',
      sname_ru_s: 'Производственные индексы',
      description_sfs_text_ru: 'Производственные индексы.',
    },
  ],
  tests: R.map(
    ({ lang, search, data }) => ({
      name: `should get a result in ${lang}`,
      request: { method: 'post', url: '/api/search', data: { search, lang } },
      results: [{ fct: fctName, data }],
    }),
    dataFTS,
  ),
}

export default fixture
