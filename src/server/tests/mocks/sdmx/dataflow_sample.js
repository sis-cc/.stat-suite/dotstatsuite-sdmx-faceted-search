module.exports = ({ id, agencyID, version = '1.0' } = {}) => ({
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    contentLanguages: ['en'],
    id: 'IDREF1',
    prepared: '2019-03-01T09:33:28.0392924Z',
    test: false,
  },
  data: {
    dataflows: [
      {
        id,
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST:DF_ILMS_ALL_MFL_TEMP_OCU_NB(1.0)',
            type: 'dataflow',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=DS1(1.0)',
            type: 'datastructure',
          },
        ],
        version,
        agencyID,
        isExternalReference: false,
        isFinal: true,
        names: { en: id },
        descriptions: {
          en: 'blah blah blah.',
        },
        annotations: [
          { type: 'LAST_UPDATE', text: '2017-02-11T11:30:30' },
          { type: 'LAYOUT_COLUMN', text: { en: 'TIME_PERIOD' } },
          { type: 'LAYOUT_ROW', text: { en: 'REF_AREA,SURVEY' } },
          { type: 'NOT_INDEXED', title: 'DIM1, DIM2=VALUE1' },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=DS1(1.0)',
      },
    ],
    contentConstraints: [
      {
        id: 'CR',
        validFrom: '2014-02-11T11:30:30',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=OECD.CFE:CR_A_DF_TOURISM_ENT_EMP(5.0)',
          },
        ],
        version: '5.0',
        agencyID: 'OECD.CFE',
        isFinal: false,
        annotations: [
          {
            title: 'A',
            type: 'ReleaseVersion',
          },
        ],
        type: 'Actual',
        constraintAttachment: {
          dataflows: [`urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=${agencyID}:${id}(${version})`],
        },
        cubeRegions: [
          {
            isIncluded: true,
            keyValues: [
              {
                id: 'DIM1',
                values: ['VALUE1'],
              },
              {
                id: 'DIM2',
                values: ['VALUE1', 'VALUE2', 'VALUE3', 'VALUE4'],
              },
              {
                id: 'DIM3',
                values: ['VALUE1'],
              },
            ],
          },
        ],
      },
    ],
    categorySchemes: [
      {
        id: 'CATS1',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=TEST:CAS_SUBJECT(1.0)',
            type: 'categoryscheme',
          },
        ],
        version: '1.0',
        agencyID: 'TEST',
        isExternalReference: false,
        isFinal: true,
        names: { en: 'CATS1' },
        categories: [
          {
            id: 'CAT1',
            names: { en: 'CAT1' },
          },
        ],
      },
    ],
    categorisations: [
      {
        id: 'CAT_DF',
        version: '1.0',
        agencyID: 'TEST',
        isExternalReference: false,
        isFinal: true,
        source: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=${agencyID}:${id}(${version})',
        target: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=TEST:CATS1(1.0).CAT1',
      },
    ],
    conceptSchemes: [
      {
        id: 'CPTS1',
        version: '1.0',
        agencyID: 'TEST',
        isExternalReference: false,
        isFinal: true,
        names: { en: 'CPTS1' },
        isPartial: true,
        concepts: [
          {
            id: 'CONCEPT1',
            names: { en: 'Concept1' },
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)' },
          },
          {
            id: 'CONCEPT2',
            names: { en: 'Concept2' },
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)' },
          },
          {
            id: 'CONCEPT3',
            names: { en: 'Concept3' },
            coreRepresentation: { enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)' },
          },
          {
            id: 'TIME_PERIOD',
            names: { en: 'Time Period' },
          },
        ],
      },
    ],
    codelists: [
      {
        id: 'CL1',
        version: '1.0',
        agencyID: 'TEST',
        isExternalReference: false,
        isFinal: true,
        names: { en: 'CL1' },
        isPartial: true,
        codes: [
          {
            id: 'VALUE1',
            names: { en: 'VALUE1' },
          },
          {
            id: 'VALUE11',
            names: { en: 'VALUE11' },
            parent: 'VALUE1',
          },
          {
            id: 'VALUE2',
            names: { en: 'VALUE2' },
            annotations: [{ type: 'NOT_INDEXED' }],
          },
          {
            id: 'VALUE3',
            names: { en: 'VALUE3' },
          },
          {
            id: 'VALUE4',
            names: { en: 'VALUE4' },
          },
        ],
      },
      {
        id: 'CL2',
        version: '1.0',
        agencyID: 'TEST',
        isExternalReference: false,
        isFinal: true,
        names: { en: 'CL2' },
        isPartial: true,
        codes: [
          {
            id: 'CL2_CODE1',
            names: { en: 'CL2_CODE1' },
          },
        ],
      },
    ],
    dataStructures: [
      {
        id: 'DS1',
        version: '1.0',
        agencyID: 'TEST',
        isExternalReference: false,
        isFinal: true,
        names: { en: 'DS1' },
        descriptions: {
          en:
            'Inflow of employed migrants refer to the number of persons who changed their country of usual residence and were also employed during a specified brief period. Data are disaggregated by occupation according to the latest version of the International Standard Classification of Occupations (ISCO-08). Information on occupation provides a description of the set of tasks and duties which are carried out by, or can be assigned to, one person.',
        },
        dataStructureComponents: {
          attributeList: {
            id: 'AttributeDescriptor',
            attributes: [
              {
                id: 'ATTR1',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CPTS1(1.0).CONCEPT1',
              },
              {
                id: 'ATTR2',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CPTS1(1.0).CONCEPT2',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)',
                },
              },
            ],
          },
          dimensionList: {
            id: 'DimensionDescriptor',
            dimensions: [
              {
                id: 'DIM1',
                position: 0,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CPTS1(1.0).CONCEPT1',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST:CL1(1.0)',
                },
              },
              {
                id: 'DIM2',
                position: 1,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CPTS1(1.0).CONCEPT2',
              },
              {
                id: 'DIM3',
                position: 1,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CPTS1(1.0).CONCEPT3',
                annotations: [{ type: 'NOT_INDEXED' }],
              },
            ],
            timeDimensions: [
              {
                id: 'TIME_PERIOD',
                position: 2,
                type: 'TimeDimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=TEST:CPTS1(1.0).TIME_PERIOD',
                localRepresentation: {
                  textFormat: { textType: 'ObservationalTimePeriod', isSequence: false, isMultiLingual: false },
                },
              },
            ],
          },
        },
      },
    ],
  },
})
