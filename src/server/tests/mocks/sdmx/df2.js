const structure = require('./unlocalized')

module.exports = {
  ...structure,
  data: {
    ...structure.data,
    dataflows: [
      structure.data.dataflows[1]
    ],
  }
}


