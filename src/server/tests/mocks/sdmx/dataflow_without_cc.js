import { over, omit, lensProp, pipe } from 'ramda'
import dataflow from './dataflow'

module.exports = ref => pipe(over(lensProp('data'), omit(['contentConstraints'])))(dataflow(ref))
