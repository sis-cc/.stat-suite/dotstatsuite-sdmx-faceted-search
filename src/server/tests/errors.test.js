import axios from 'axios'
import nock from 'nock'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import fixture from './fixtures/data1'
import { manageSchema, tearContext, initConfig } from './utils'
import initMongo from '../init/mongo'

let TENANT

const IRS = require('./mocks/sdmx/IRS')

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } })
    .then(res => [res.status, res.data])
    .catch(err => {
      if (err.response) return [err.response.status, err.response.data]
      return []
    })

let makeRequest
let CTX
const tc = () => CTX

const params = () => ({
  defaultLocale: 'en',
})

const initContext = async () => {
  try {
    CTX = await initConfig(createContext, params())
      .then(initMongo)
      .then(ctx => {
        TENANT = ctx().TENANT
        return ctx
      })
      .then(initReporter)
      .then(initSolr)
      .then(async ctx => {
        await manageSchema(TENANT, ctx().solr)
        return ctx
      })
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    // await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext(tc))

describe('Server | services | admin', () => {
  describe('dataflow', () => {
    it('should return 404', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]
      const { dataflowId_s: id, version_s: version, agencyId_s: agencyId } = fixture.data[0]
      const dataflows = [{ id, agencyId, version }]

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(404, IRS)

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(404)

      const [status, res] = await makeRequest({
        method: 'POST',
        url: '/admin/dataflow?mode=sync',
        data: { dataSpaceId, id, version, agencyId },
      })
      expect(status).toEqual(404)
    })

    it('should return 200', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]
      const { dataflowId_s: id, version_s: version, agencyId_s: agencyId } = fixture.data[0]
      const dataflows = [{ id, agencyId, version }]

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(200, IRS)

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(200, IRS)

      const [status, res] = await makeRequest({
        method: 'POST',
        url: '/admin/dataflow?mode=sync',
        data: { dataSpaceId, id, version, agencyId },
      })
      expect(res[0]['demo-stable'].dataflows.loaded).toEqual(1)
      expect(status).toEqual(200)
    })

    it('should return 207', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]
      const { dataflowId_s: id, version_s: version, agencyId_s: agencyId } = fixture.data[0]
      const dataflows = [{ id, agencyId, version }]

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
        .reply(200, { data: {} })

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(200, IRS)

      const [status, res] = await makeRequest({
        method: 'POST',
        url: '/admin/dataflow?mode=sync',
        data: { dataSpaceId, id, version, agencyId },
      })
      expect(res[0]['demo-stable'].dataflows.loaded).toEqual(1)
      expect(status).toEqual(207)
    })

    it('should return 207', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]
      const { dataflowId_s: id, version_s: version, agencyId_s: agencyId } = fixture.data[0]
      const dataflows = [{ id, agencyId, version }]

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
        .reply(404)

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(200, IRS)

      const [status, res] = await makeRequest({
        method: 'POST',
        url: '/admin/dataflow?mode=sync',
        data: { dataSpaceId, id, version, agencyId },
      })
      expect(res[0]['demo-stable'].dataflows.loaded).toEqual(1)
      expect(status).toEqual(207)
    })

    it('should return 409 and not create a dataflow', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]
      const { dataflowId_s: id, version_s: version, agencyId_s: agencyId } = fixture.data[0]

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
        .reply(404)

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
        .reply(404)

      const [status] = await makeRequest({
        method: 'POST',
        url: '/admin/dataflow?mode=sync',
        data: { dataSpaceId, id, version, agencyId },
      })
      expect(status).toEqual(409)
    })
  })

  describe('dataflows', () => {
    it('should return 207', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]
      const { dataflowId_s: id, version_s: version, agencyId_s: agencyID } = fixture.data[0]
      const dataflows = [{ id, agencyID, version }]

      nock(TENANT.spaces['qa-stable'].url)
        .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
        .reply(404)

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0/?references=dataflow')
        // .reply(404)
        .reply(200, { data: {} })

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
        // .reply(404)
        .reply(200, { data: { dataflows } })

      nock(space.url)
        .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
        .reply(200, IRS)

      const email = 'toto@titi.fr'
      const [status] = await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&userEmail=${email}` })
      expect(status).toEqual(207)
    })

    it('should return 409', async () => {
      const dataSpaceId = 'demo-stable'
      const space = TENANT.spaces[dataSpaceId]

      nock(TENANT.spaces['qa-stable'].url)
        .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
        .reply(404)

      nock(space.url)
        .get('/categoryscheme/OECD/TEST/1.0/?references=dataflow')
        .reply(404)

      nock(space.url)
        .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
        .reply(404)

      const email = 'toto@titi.fr'
      const [status] = await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&userEmail=${email}` })
      expect(status).toEqual(409)
    })
  })
})
