import Loader from '..'

jest.mock('axios', () =>
  jest.fn(({ url }) => {
    const categoryscheme = require('../../tests/mocks/sdmx/categoryscheme.js')
    if (/categoryscheme/.test(url)) {
      return Promise.resolve({ status: 200, data: categoryscheme })
    }
    return Promise.reject({ status: 400 })
    // return Promise.resolve({ status: 400 })
    // also works because only 200 and 206 are not rejected
  }),
)

const tenants = {
  spaces: {
    test: {
      url: 'https://www.ilo.org/sdmx/rest',
    },
  },
  datasources: {
    OECD: {
      dataSpaceId: 'test',
      indexed: true,
      dataqueries: [
        {
          agencyId: 'OECD',
          categorySchemeId: 'OECDCS1',
          version: '1.0',
        },
        {
          agencyId: 'OECD',
          categorySchemeId: 'OECDCS2',
          version: '1.0',
        },
      ],
    },
  },
}

describe('Dataflow Loader', () => {
  it('should not load', async () => {
    const loader = Loader({ dimensionValuesLimit: 1000 })

    let errors = 0

    loader.on('done', () => {
      expect(errors).toEqual(123 * 2)
    })

    loader.on('error', () => errors++)

    /* eslint-disable no-unused-vars */
    /* eslint-disable no-empty */
    for await (const d of loader(tenants, 1)) {
    }
  })
})
