import ConfigManager from '../configManager'

export default async ctx => {
  const configManager = await ConfigManager(ctx)
  return ctx({ configManager })
}
