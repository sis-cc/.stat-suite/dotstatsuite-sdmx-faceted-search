import { merge } from 'ramda'
import debug from '../../../debug'

const getTenant = ctx => async (req, res, next) => {
  const {
    config: { defaultTenant },
    configProvider,
  } = ctx()
  const tenantId = req.query.tenant || req.headers['x-tenant'] || defaultTenant
  if (!tenantId) return next()

  try {
    const t0 = new Date()

    const [tenant, settings] = await Promise.all([
      configProvider.getTenant(tenantId),
      configProvider.getSettings(tenantId, { default: null }),
    ])
    req.tenant = tenant

    if (settings) {
      for (const spaceId in settings.spaces || {})
        if (tenant.spaces?.[spaceId]) {
          if (settings.spaces[spaceId].url) tenant.spaces[spaceId].url = settings.spaces[spaceId].url
          if (settings.spaces[spaceId].headers)
            tenant.spaces[spaceId].headers = merge(tenant.spaces[spaceId].headers, settings.spaces[spaceId].headers)
        }
    }

    debug.info(`load tenant '${req.tenant?.id}' in ${new Date() - t0} ms`)
    next()
  } catch (err) {
    next(err)
  }
}

export default getTenant
