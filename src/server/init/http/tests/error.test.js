import axios from 'axios';
import initHttp from '..';
import { initConfig } from '../../../tests/utils';
import initService from '../../../services';

let CTX;

describe('Http Error middleware', function() {
  beforeAll(function() {
    return initConfig()
      .then(ctx => ctx({ ...ctx, services: {} }))
      .then(initService)
      .then(initHttp)
      .then(ctx => (CTX = ctx));
  });

  afterAll(() => CTX().http.close());

  it('should throw error', done => {
    const url = `${CTX().http.url}/api/search`;
    axios({ method: 'GET', url }).catch(() => done());
  });
});
