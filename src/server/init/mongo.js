import { MongoClient } from 'mongodb'
import debug from '../debug'

export default async ctx => {
  const {
    config: {
      mongo: { dbName, url },
    },
  } = ctx()
  const mongoClient = new MongoClient(url, { readPreference: 'nearest' })
  await mongoClient.connect()
  debug.info('connected to mongodb server')
  return ctx({
    mongo: {
      client: mongoClient,
      database: mongoClient.db(dbName),
      ping: () => mongoClient.db(dbName).command({ ping: 1 }),
      close: () => mongoClient.close(),
      dropDatabase: dbName => mongoClient.db(dbName).dropDatabase(),
    },
  })
}
