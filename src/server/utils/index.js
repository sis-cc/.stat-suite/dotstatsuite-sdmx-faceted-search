import { is, map, propEq, compose, trim, reduce, includes, pipe, toPairs, ifElse, assoc } from 'ramda';
import bs52 from 'bs52';
import ASCIIFolding from 'fold-to-ascii';

export const encode = compose(x => bs52.encode(x), trim);
export const decode = x => bs52.decode(x);
export const isTestEnv = propEq('env', 'test');

export const filterMemberCollections = (members, collections) => {
  const memberList = Object.keys(members);
  return reduce(
    (acc, col) => {
      if (includes(col, memberList)) return { ...acc, [col]: collections[col] };
      return acc;
    },
    {},
    Object.keys(collections),
  );
};

export const fold = value => ASCIIFolding.foldReplacing(value);
export const foldToAscii = json => {
  return pipe(
    toPairs,
    reduce((acc, [key, values]) => {
      const foldValues = ifElse(is(Array), map(fold), fold)(values);
      return assoc(fold(key), foldValues, acc);
    }, {}),
  )(json);
};
