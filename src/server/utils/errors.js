function HTTPError(code, message) {
  Error.captureStackTrace(this, HTTPError);
  this.message = message || `HTTP Error code ${code}`;
  this.name = 'HTTPError';
  this.code = code;
}

HTTPError.prototype = Object.create(Error.prototype);
HTTPError.prototype.constructor = HTTPError;

function ClientNotFoundError(id) {
  Error.captureStackTrace(this, ClientNotFoundError);
  this.message = `Client#${id} not found`;
  this.name = 'ClientNotFoundError';
  this.id = id;
}

ClientNotFoundError.prototype = Object.create(Error.prototype);
ClientNotFoundError.prototype.constructor = ClientNotFoundError;

function SessionTimedout() {
  Error.captureStackTrace(this, SessionTimedout);
  this.name = 'SessionTimedout';
}

SessionTimedout.prototype = Object.create(Error.prototype);
SessionTimedout.prototype.constructor = SessionTimedout;


class NoActiveDatasources extends Error{}
class WrongLoadingSettings extends Error{}

module.exports = { WrongLoadingSettings, NoActiveDatasources, SessionTimedout, ClientNotFoundError, HTTPError };
